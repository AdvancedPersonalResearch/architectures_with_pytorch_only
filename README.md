# Collection of various Deep learning architectures programmed with Pytorch only

This repository consists of the re-implementation of popular deep convolutional neural networks architectures I have programmed with Pytorch.

## Getting start

 001-AlexNet architecture

 002-VGG16 architecture

 003-VGG19 architecture

 004-GoogleNet (Inception v1) architecture

 005-MobileNet architecture v1

 006-Xception architecture

 007-Unet architecture
 
 008-Resnet architecture

 009-The 'Discriminator architecture' and the 'Generator architecture' for the BEGAN architecture 
 
 010-Feature Pyramid Network architecture 
 
 011-HMTNet "Hierarchical Multi-task Network" architecture

 012-(coming soon)

